#![warn(clippy::all)]
#![warn(clippy::pedantic)]
#![warn(clippy::nursery)]

fn get_missing_nums(arr: &[u32]) -> Vec<u32> {
    let mut x: Vec<u32> = Vec::new();
    let mut add: u32 = *arr.first().expect("The array is empty!") - 1;

    for mut i in 0..*arr.last().expect("The array is empty!") {
        let v = *match arr.get(i as usize) {
            Some(c) => c,
            None => break,
        };
        i += 1 + add;

        if i != v {
            for k in 0..(v - i) {
                add += 1;
                x.push(i + k);
            }
        }
    }

    x
}

fn main() {
    let arr = [10, 11, 12, 14, 15, 16, 17, 18, 19, 20, 22, 24, 25, 26, 27, 30, 32, 33, 34, 35, 36, 45, 55];

    println!("Nums: {:?}!", get_missing_nums(&arr));
}

#[cfg(test)]
mod test {
    use crate::get_missing_nums;

    #[test]
    fn potr() {
        let arr = [10, 11, 12, 14, 15, 16, 17, 18, 19, 20, 22, 24, 25, 26, 27, 30, 32, 33, 34, 35, 36, 45, 55];

        assert_eq!(
            get_missing_nums(&arr),
            [13, 21, 23, 28, 29, 31, 37, 38, 39, 40, 41, 42, 43, 44, 46, 47, 48, 49, 50, 51, 52, 53, 54],
            "The found missing numbers are wrong!"
        );
    }
}
